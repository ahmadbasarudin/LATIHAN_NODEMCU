#define TINY_GSM_MODEM_SIM800

#include <SoftwareSerial.h>
#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include "DHT.h"
#include <Wire.h>  // This library is already built in to the Arduino IDE
#include <LiquidCrystal_I2C.h> //This library you can add via Include Library > Manage Library > 

#define DHTPIN                          D5
#define DHTTYPE                         DHT11
#define MODEM_RX                        D8
#define MODEM_TX                        D7
bool    USE_LCD                 =       true;

//#define DUMP_AT_COMMANDS

const int     BAUDRATE          =       9600;


const char GPRS_APN[]      = "www.indosat-m3.net"; // APN (example: internet.vodafone.pt) use https://wiki.apnchanger.org
const char GPRS_USER[] = "gprs"; // GPRS User
const char GPRS_PASS[] = "im3"; // GPRS 
//const char GPRS_APN[]           =       "3gprs"; // APN (example: internet.vodafone.pt) use https://wiki.apnchanger.org
//const char GPRS_USER[]          =       "3gprs"; // GPRS User
//const char GPRS_PASS[]          =       "3gprs"; // GPRS


//const char*   MQTT_HOST         =       "18.141.55.139";
const char*   MQTT_HOST         =       "demo.thingsboard.io";
//const char*   MQTT_HOST         =       "node02.myqtthub.com";
const int     MQTT_PORT         =       1883;
const char*   MQTT_CLIENT       =       "basardevice";
//const char*   MQTT_USER         =       "nodemcu";
const char*   MQTT_USER         =       "THIcq1O0O5QjcD598RME";
const char*   MQTT_PASS         =       "";
//const char    MQTT_SUB_TOPIC[]  =       "rumah/suhu";
//const char    MQTT_PUB_TOPIC[]  =       "rumah";

const char    MQTT_PUB_TOPIC[]  =       "v1/devices/me/telemetry";
const char    MQTT_SUB_TOPIC[]  =       "v1/devices/me/attributes";


//waktu yang diperlukan ambil data setiap 1 detik
#define       TIME_FECTH_DATA           1000 
//waktu yang dibutuhkan untuk looping kembali
#define       TIME_LOOP_PROCESS         5000 
#define       MQTT_KEEPALIVE            60



float kelembaban =0;
float suhu = 0;
String data = "";
unsigned long interval=1000; // the time we need to wait
unsigned long previousMillis=0; // millis() returns an unsigned long.

LiquidCrystal_I2C lcd(0x27, 16, 2);
DHT dht(DHTPIN, DHTTYPE);
SoftwareSerial SerialAT(MODEM_RX,MODEM_TX); // RX,TX
#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(SerialAT, Serial);
  TinyGsm modem(debugger);
#else
  TinyGsm modem(SerialAT);
#endif

TinyGsmClient client(modem);
PubSubClient mqtt(client);

boolean mqttConnect()
{
  if(!mqtt.connect(MQTT_CLIENT,MQTT_USER,MQTT_PASS))
  //if(!mqtt.connect(MQTT_CLIENT))
  {
    Serial.print(".");      
    if(USE_LCD)
    {
      lcd.print(".");
    }
    return false;
  }
  Serial.println("terkoneksi  ke MQTT broker.");     
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("MQTT terkoneksi");  
  }
  mqtt.subscribe(MQTT_SUB_TOPIC);
  return mqtt.connected();
}

void mqttCallback(char* topic, byte* payload, unsigned int len)
{
  
  Serial.print("Pesan diterima: ");
  Serial.write(payload, len);    
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("Pesan diterima:");  
    lcd.setCursor(0,1);
    lcd.print((char *)payload);  
  }
  Serial.println();

      if( !strncmp((char *)payload, "unlocked", len) )
      {
        Serial.print("Unlocked");
        //digitalWrite(LOCKPIN, LOW); 
        mqtt.publish(MQTT_PUB_TOPIC, "Unlocked");
       } 
      else if( !strncmp((char *)payload, "locked", len)  )
      {
        Serial.print("Locked");
        //digitalWrite(LOCKPIN, HIGH); 
        mqtt.publish(MQTT_PUB_TOPIC, "Locked");

       } 
      else if( !strncmp((char *)payload, "reset", len)  )
      {
        Serial.print("menerima perintah reset");
        modul_reset();

       }                     
}
void modul_reset()
{
  
  Serial.println("Reset modem...");  
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("Reset modem...");  
  }
  
  modem.restart();
  delay(6000);
  
  Serial.println("Reset SOC...");
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("Reset SOC...");  
  }
  ESP.restart(); //ESP.reset();
}
void read_data()
{
  kelembaban = dht.readHumidity();
  suhu = dht.readTemperature();
  
  Serial.print("\n");
  Serial.print(F("Humidity: "));
  Serial.print(kelembaban);
  Serial.print("\t");
  Serial.print(F("  Temperature: "));
  Serial.print(suhu);
  Serial.print(F("°C "));

  if(USE_LCD)
  {
    lcd.clear();
      
    lcd.print("Temp:  Humidity:");
    if (isnan(kelembaban) || isnan(suhu)) {
      lcd.print("ERROR");
      return;
    }
    lcd.setCursor(0,1);
    lcd.print(suhu);
    lcd.setCursor(7,1);
    lcd.print(kelembaban); 
    lcd.print("    "); 
  }

  delay(TIME_FECTH_DATA);
}

void publish_data()
{
  
  //mqtt.publish(MQTT_PUB_SUHU,String(suhu).c_str(), true);
  //delay(1000);
  //mqtt.publish(MQTT_PUB_KELEMBAPAN,String(kelembaban).c_str(), true);

  StaticJsonDocument<200> doc;
  char buffer[256];

  

  
  //data temperatur
  //data kelembaban
  doc["kelembaban"] = kelembaban;
  doc["suhu"] = suhu;
  
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("publish data ke ");  
    lcd.setCursor(0,1);
    lcd.print(MQTT_HOST);  
  }
  
  size_t n = serializeJson(doc, buffer);
  mqtt.publish(MQTT_PUB_TOPIC, buffer, n);

}

void lcd_scan()
{
  byte error, address; //variable for error and I2C address
  int n_devices;

  Serial.println("Scanning LCD...");

  n_devices = 0;
  for (address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("  !");
      n_devices++;
    }
    else if (error == 4)
    {
      Serial.print("Unknown error at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  if (n_devices == 0)
  {
    Serial.println("No I2C devices found\n");
    USE_LCD = false;
  }
  else
  {
    Serial.println("done\n");
  }

  delay(5000); // wait 5 seconds for the next I2C scan
}
void setup(){
  Serial.begin(BAUDRATE);
  Serial.println("Memulai sistem.");

  
  //lcd_scan();
  if(USE_LCD)
  {
    //Wire.begin(2,0);
    lcd.init();   // initializing the LCD
    lcd.backlight(); // Enable or Turn On the backlight 
    lcd.print("Memulai... "); // Start 
    Serial.print("menggunakan lcd");
  }
  Serial.print("lcd siap");
  
  SerialAT.begin(BAUDRATE);
  modem.restart();
  
  String modem_name = modem.getModemInfo();
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("Modem: " +modem_name.substring(0, 9));
    lcd.setCursor(0,1);
    lcd.print("Mencari operator...");  
  }
  
  Serial.println("Modem: " + modem.getModemInfo());
  Serial.println("Mencari operator.");
  
  if(!modem.waitForNetwork(60000L))
  {
    Serial.println("operator tidak terhubung");
    if(USE_LCD)
    {
      lcd.clear();
      lcd.print("operator tidak terhubung");
      lcd.setCursor(0,1);
      lcd.print("modem akan direset");  
    }
    modul_reset();
    while(true);
  }
  
  Serial.println("terhubung  " + modem.getOperator());
  Serial.println("Sinyal: " + String(modem.getSignalQuality()));
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print(modem.getOperator() + " :: "+ String(modem.getSignalQuality()));
    lcd.setCursor(0,1);
    lcd.print("Menghubungkan  GPRS...");
  }
  Serial.println("Menghubungkan jaringan GPRS.");
  if (!modem.gprsConnect(GPRS_APN, GPRS_USER, GPRS_PASS))
  {
    Serial.println("GPRS terputus");
    if(USE_LCD)
    {
      lcd.clear();
      lcd.print("GPRS terputus");
      lcd.setCursor(0,1);
      lcd.print("reset modul");
    }
    modul_reset();
    while(true);
  }

  
  if (modem.isGprsConnected()) 
  {
    Serial.println("GPRS sudah terhubung: " + String(GPRS_APN));
    if(USE_LCD)
    {
      lcd.clear();
      lcd.print("GPRS sudah terhubung: " + String(GPRS_APN));
    }
  }
  
  mqtt.setServer(MQTT_HOST, MQTT_PORT);
  mqtt.setCallback(mqttCallback);
  Serial.println("Connecting to Broker: " + String(MQTT_HOST));
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("connecting:");
    lcd.setCursor(0,1);
    lcd.print(String(MQTT_HOST) + " ...");
  }

  uint32_t t = millis();
  while(mqttConnect()==false)
  {
    if(t > 50000L)
    {
      modul_reset();
    }
    else
    {
      continue;
    }
  }
  Serial.println();
  dht.begin();
}

void loop()
{
  
 unsigned long currentMillis = millis(); // grab current time
  /*
  if(Serial.available())
  {
    delay(10);
    String message="";
    while(Serial.available()) message+=(char)Serial.read();
    mqtt.publish(MQTT_PUB_TOPIC, message.c_str());
  }
  */
  
   // check if "interval" time has passed (1000 milliseconds)
   if ((unsigned long)(currentMillis - previousMillis) >= interval) {
     
    read_data();
    publish_data();
    previousMillis = millis();
   }
  if(mqtt.connected())
  {
    mqtt.loop();
    
  }
  else
  {
    //mereset modem dan mikrokontroller
    modul_reset();
  }
}
