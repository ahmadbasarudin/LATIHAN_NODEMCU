
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
//https://pubsubclient.knolleary.net
#include "DHT.h"
#include <ArduinoJson.h>
//https://github.com/bblanchon/ArduinoJson

#ifndef STASSID
#define WIFI_SSID "macbook"
#define WIFI_PASSWORD  "12345678"

#define DHT_PIN D7
#define DHT_TYPE DHT11

#define MQTT_TOKEN "b3a4307d68f4b2332fb6f71eaa9a99ad"
#define MQTT_ID "MQTT_CLIENT_ID"
#define MQTT_SERVER "192.168.43.156"
#define MQTT_PORT 1883

#define LED_LAMP LED_BUILTIN
//sementara belum support keepalive

//waktu kedip mengkoneksikan ke wifi
#define WIFI_LED_INTERVAL_CONNECTING  500
//waktu yang diperlukan untuk terkoneksi mqtt kembali yang sebelumnya terputus
#define TIME_MQTT_RECONNECTING  5000

//waktu yang diperlukan ambil data setiap 0.5 detik
#define TIME_FECTH_DATA 500 
//waktu yang dibutuhkan untuk looping kembali
#define TIME_LOOP_PROCESS 2000 
#endif





boolean wifi_state = false;

const char* publish_topic = "DEVICE/UNSECURE";
const char* subscribe_topic = MQTT_ID;

float kelembaban =0;
float suhu = 0;


WiFiClient esp_client;
PubSubClient client(esp_client);


DHT dht(DHT_PIN, DHT_TYPE);

void setup() {
  Serial.begin(115200);
  pinMode(LED_LAMP, OUTPUT);
  setup_wifi();
  
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);
  
}


void setup_wifi() 
{
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  digitalWrite(LED_LAMP, LOW);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(WIFI_LED_INTERVAL_CONNECTING);
    Serial.print(".");
    
    wifi_state = !wifi_state; 
    digitalWrite(LED_LAMP, wifi_state);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
}
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void read_data()
{
  kelembaban = dht.readHumidity();
  suhu = dht.readTemperature();
  
  Serial.print("\n");
  Serial.print(F("Humidity: "));
  Serial.print(kelembaban);
  Serial.print("\t");
  Serial.print(F("  Temperature: "));
  Serial.print(suhu);
  Serial.print(F("°C "));

  delay(TIME_FECTH_DATA);
}

void publish_data()
{

  StaticJsonDocument<200> doc;
  char buffer[256];

  doc["token"] = MQTT_TOKEN;
  doc["timestamp"] = 0; //belum support timestamp mandiri
  
  //data kelembaban
  doc["sensor-id"] = 1;
  doc["value"] = kelembaban;

  size_t n = serializeJson(doc, buffer);
  client.publish(publish_topic, buffer, n);
  
  //data temperatur
  doc["sensor-id"] = 2;
  doc["value"] = suhu;
  n = serializeJson(doc, buffer);
  client.publish(publish_topic, buffer, n);

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    // Attempt to connect
    if (client.connect(MQTT_ID) )
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(publish_topic, "hello world");
      // ... and resubscribe
      client.subscribe(subscribe_topic);
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(TIME_MQTT_RECONNECTING);
    }
  }
}
void loop() 
{

  if (!client.connected()) 
  {
    reconnect();
  }
  client.loop();
  read_data();
  publish_data();
  delay(TIME_LOOP_PROCESS);
}
