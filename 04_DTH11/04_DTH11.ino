#include "DHT.h"
#include <Wire.h>  // This library is already built in to the Arduino IDE
#include <LiquidCrystal_I2C.h> //This library you can add via Include Library > Manage Library > 

#define DHTPIN 5
#define DHTTYPE DHT11
LiquidCrystal_I2C lcd(0x27, 16, 2);
DHT dht(DHTPIN, DHTTYPE);


const bool   USE_LCD = false;

void setup(){
  Serial.begin(115200);
  Serial.print("mulai");
  if(USE_LCD)
  {
    //Wire.begin(2,0);
    lcd.init();   // initializing the LCD
    lcd.backlight(); // Enable or Turn On the backlight 
    lcd.print("Memulai... "); // Start 
  }
  Serial.print("lcd siap");
  dht.begin();
}
void loop()
{ 
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  
  if(USE_LCD)
  {
    lcd.clear();
      
    lcd.print("Temp:  Humidity:");
    if (isnan(h) || isnan(t)) {
      lcd.print("ERROR");
      return;
    }
    lcd.setCursor(0,1);
    lcd.print(t);
    lcd.setCursor(7,1);
    lcd.print(h); 
    lcd.print("    "); 
  }
  
  Serial.print("\n");
  Serial.print(F("Humidity: "));
  Serial.print(h);
  
  //lcd.print("Memulai... "); // Start 
  Serial.print("\t");
  Serial.print(F("  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  
  delay(1000);
}
